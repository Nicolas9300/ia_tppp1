from flask import Flask
from threading import Thread
from dengue import calcular_probabilidad

app = Flask('')


def run():
  app.run(host='0.0.0.0', port=8080)


def keep_alive():
  t = Thread(target=run)
  t.start()


@app.route("/")
def index():
  return "Esta funcionando esto"


@app.get("/predict/{provincia_id}/{grupo_edad_id}")
async def getPrediction(provincia_id, grupo_edad_id):
  try:
    resultado = calcular_probabilidad(int(provincia_id), int(grupo_edad_id))
    return {
        "porcentaje": f"{resultado['probabilidad']}",
        "cantidad_casos": f"{resultado['casos']}"
    }
  except Exception as e:
    return {"error": str(e)}


keep_alive()
