import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score, silhouette_samples


# Función para calcular la probabilidad de contagio
def calcular_probabilidad_contagio(data, provincia_id, grupo_edad_id):
  filtro_provincia = data["provincia_id"] == provincia_id
  casos_en_provincia = data.loc[filtro_provincia, "cantidad_casos"].sum()

  filtro_provincia_grupo_edad = (data["provincia_id"] == provincia_id) & (
      data["grupo_edad_id"] == grupo_edad_id)
  casos_en_provincia_grupo_edad = data.loc[filtro_provincia_grupo_edad,
                                           "cantidad_casos"].sum()

  resultado = {}

  if casos_en_provincia > 0:
    probabilidad_contagio = (casos_en_provincia_grupo_edad /
                             casos_en_provincia) * 100
    resultado["probabilidad"] = probabilidad_contagio
    resultado["casos"] = casos_en_provincia
  else:
    resultado[
        "mensaje"] = f"No hay datos disponibles para la provincia {provincia_id}."

  return resultado


# Función para realizar el clustering y calcular el coeficiente Silhouette
def clusterizar_y_calcular_silhouette(data):
  grouped_data = data.groupby(["provincia_id", "grupo_edad_id"
                               ]).sum()["cantidad_casos"].reset_index()
  X = grouped_data[["provincia_id", "grupo_edad_id", "cantidad_casos"]].values
  num_clusters = 3
  kmeans = KMeans(n_clusters=num_clusters)
  kmeans.fit(X)
  labels = kmeans.labels_
  silhouette_avg = silhouette_score(X, labels)
  sample_silhouette_values = silhouette_samples(X, labels)
  grouped_data["silhouette_value"] = sample_silhouette_values
  return grouped_data, silhouette_avg


# Función principal
def main():
  # Lectura de datos desde un archivo CSV
  data = pd.read_csv("archivo-dengue-completo-2023-09-02.csv")

  # Realizar el clustering y calcular el coeficiente Silhouette
  grouped_data, silhouette_avg = clusterizar_y_calcular_silhouette(data)
  print("Coeficiente Silhouette:", silhouette_avg)

  # Provincia y grupo de edad específicos para el cálculo de probabilidad de contagio
  provincia_id = 28
  grupo_edad_id = 7
  resultado = calcular_probabilidad_contagio(data, provincia_id, grupo_edad_id)
  print(f"Resultado: {resultado}")

  # Imprimir el resultado de la probabilidad de contagio
  if "probabilidad_contagio" in resultado:
    print(
        f"La probabilidad de contraer un contagio en la provincia {provincia_id} es del {resultado['probabilidad']:.2f}%, con un total de {resultado['casos']} casos."
    )
  else:
    print(resultado["mensaje"])


def calcular_probabilidad(provincia_id, grupo_edad_id):
  # Lectura de datos desde un archivo CSV
  data = pd.read_csv("archivo-dengue-completo-2023-09-02.csv")

  # Realizar el clustering y calcular el coeficiente Silhouette
  grouped_data, silhouette_avg = clusterizar_y_calcular_silhouette(data)

  resultado = calcular_probabilidad_contagio(data, provincia_id, grupo_edad_id)

  return resultado


if __name__ == "__main__":
  main()
